﻿using Badge.Plugin;
using SBVC.Mobile.Pages.Autos;
using SBVC.Mobile.Pages.Bot;
using SBVC.Mobile.Pages.Buscador;
using SBVC.Mobile.Pages.Documentacion;
using SBVC.Mobile.Pages.Expenses;
using SBVC.Mobile.Pages.Hoteles;
using SBVC.Mobile.Pages.MisPendientes;
using SBVC.Mobile.Pages.MisViajes;
using SBVC.Mobile.Pages.Notificaciones;
using SBVC.Mobile.Pages.Perfil;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBVC.Mobile.Pages.MenuView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : MasterDetailPage
    {
        public MenuPage()
        {
            InitializeComponent();
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
            var notificaciones = new ToolbarItem
            {
                Icon = "bellwhite.png",
                Command = new Command(ShowSettingsPage)
            };
            
            //CrossBadge.Current.SetBadge(10);
            Detail.ToolbarItems.Add(notificaciones);
        }
        async private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            IsPresented = false;
            await Task.Delay(500);
            var item = e.SelectedItem as MenuPageMenuItem;
            if (item == null)
                return;

            var page = (Page)null;
            switch (item.Title)
            {
                case "Autos":
                    page = new AutosPage();
                    page.Title = item.Title;                  
                    break;
                case "Buscador":
                    page = new BuscadorPage();
                    page.Title = item.Title;
                    page.Icon = "fa-bell";
                    break;
                case "Mis Viajes":
                    page = new MisViajesWebPage();
                    page.Title = item.Title;
                    break;
                case "Mis Autorizaciones":
                    page = new MisPendientesPage();
                    page.Title = item.Title;
                    break;
                case "Mi Perfil":
                    page = new PerfilPage();
                    page.Title = item.Title;
                    break;
                case "Documentacion":
                    page = new DocumentacionPage();
                    page.Title = item.Title;
                    break;
                case "Hoteles":
                    page = new HotelesPage();
                    page.Title = item.Title;
                    break;
                case "Expenses":
                    page = new ExpensesPage();
                    page.Title = item.Title;
                    break;
                case "Soporte":
                    page = new ChatBotPage();
                    page.Title = item.Title;
                    break;
                default:
                    throw new Exception();
                    break;
            }
            Detail = new NavigationPage(page);

            if (Detail.ToolbarItems.Count<=0) {
                var notificaciones = new ToolbarItem
                {
                    Icon = "bellwhite.png",
                    Command = new Command(ShowSettingsPage)
                };
                //CrossBadge.Current.SetBadge(10);
                Detail.ToolbarItems.Add(notificaciones);
            }
            MasterPage.ListView.SelectedItem = null;
        }

        private void ShowSettingsPage()
        {
            this.Navigation.PushModalAsync(new NotificacionesPage());
        }

    }
}