﻿using SBVC.Mobile.Pages.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamanimation;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBVC.Mobile.Pages.Splash
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Splash : ContentPage
    {
        public Splash()
        {
            InitializeComponent();
            this.splashBg.Source = ImageSource.FromResource("SBVC.Mobile.Resources.Images.splashBg.png");
            this.logoAviatur.Source = ImageSource.FromResource("SBVC.Mobile.Resources.Images.Logos.logoAviatur.png");
            this.logoAviatur.Margin = new Thickness(20);
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            DelayedNaviagition();
        }

        private async void DelayedNaviagition()
        {
            Task.Run(() => this.logoAviatur.Animate(new HeartAnimation()));
            await Task.Delay(3000);
            await Navigation.PushAsync(new LoginPage());
        }
    }
}