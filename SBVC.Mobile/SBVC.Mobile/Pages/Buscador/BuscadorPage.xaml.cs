﻿using Newtonsoft.Json.Linq;
using SBVC.Mobile.Pages.Notificaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBVC.Mobile.Pages.Buscador
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BuscadorPage : ContentPage
    {
        public BuscadorPage()
        {
            InitializeComponent();

            //var notificaciones = new ToolbarItem
            //{
            //    Icon = "bellwhite.png",
            //    Name = "notificaciones",
            //    Command = new Command(ShowSettingsPage)

            //};

            ////Navigation.PushModalAsync(new NotificacionesPage());
            //this.ToolbarItems.Add(notificaciones);
        }

        private void ShowSettingsPage()
        {
            this.Navigation.PushModalAsync(new NotificacionesPage());
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            JObject rss = (JObject)Application.Current.Properties["user"];
            corpUrl.Source = "https://sbvcwebmobile.azurewebsites.net/Home/SSO3/?Token=" + ((string)rss["UsuaDocu"]) + "&navBar=0";            
            try
            {
                this.Navigation.RemovePage(Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
            }
            catch (Exception ex) { }
        }
        void webOnNavigating(object sender, WebNavigatingEventArgs e)
        {           
            indicator.IsVisible = true;
        }

        void listaNots(object sender, WebNavigatingEventArgs e)
        {           
            indicator.IsVisible = true;
        }

        void webOnEndNavigating(object sender, WebNavigatedEventArgs e)
        {          
            indicator.IsVisible = false;
            corpUrl.VerticalOptions = LayoutOptions.FillAndExpand;
            corpUrl.HorizontalOptions = LayoutOptions.FillAndExpand;
            
            this.BindingContext = this;
        }
    }
}