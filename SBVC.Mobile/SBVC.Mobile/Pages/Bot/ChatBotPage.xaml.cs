﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBVC.Mobile.Pages.Bot
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatBotPage : ContentPage
    {
        public ChatBotPage()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            corpUrl.Source = "https://webchat.botframework.com/embed/Aliza?s=n3LWoYyUvUc.cwA.NcE.fVSDuZWkEQN8CZplmgI5B19D499MOLWhtldyrH_Mio4";
            //try
            //{
            //    this.Navigation.RemovePage(Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
            //}
            //catch (Exception ex) { }
        }
    }
}