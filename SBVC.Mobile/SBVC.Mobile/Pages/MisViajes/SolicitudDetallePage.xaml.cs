﻿using SBVC.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBVC.Mobile.Pages.MisViajes
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SolicitudDetallePage : ContentPage
    {
        public SolicitudesViewModel Solicitud { get; set; }
        public SolicitudDetallePage()
        {
            InitializeComponent();
            this.bgMisViajes.Source = ImageSource.FromResource("SBVC.Mobile.Shared.Resources.Images.loginbg3.png");
            this.bgMisViajes.HorizontalOptions = LayoutOptions.FillAndExpand;
            this.bgMisViajes.VerticalOptions = LayoutOptions.FillAndExpand;
            this.frame.Margin = new Thickness(30, 30, 30, 0);
            this.detail.Margin = new Thickness(30, 0, 30, 30);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            this.imagen.Source = Solicitud.SoliImga;
            this.iataOrigen.Text = Solicitud.IataOrigen;
            this.iataDestino.Text = Solicitud.IataDestino;
            this.ciudadOrigen.Text = Solicitud.CiudadOrigen;
            this.ciudadDestino.Text = Solicitud.CiudadDestino;
            this.horaOrigen.Text = Solicitud.HoraOrigen;
            this.horaDestino.Text = Solicitud.HoraDestino;
            this.fecha.Text = Solicitud.FechaOrigen;

            this.motivo.Text = Solicitud.SoliMoti;
            this.soliMoti.Text = Solicitud.SoliMoti;
            this.soliId.Text = Solicitud.SoliId.ToString();
            this.fechaSoli.Text = Solicitud.SoliFech.ToString();

            this.soliPoli.Text = Solicitud.SoliPoli;
            this.soliRese.Text = Solicitud.SoliRese;
            //this.soliEsta.Text = Solicitud.SoliEsta.ToString();
            this.solicont.Text = Solicitud.SoliCont;
            this.soliSoli.Text = Solicitud.SoliSoli;
            this.soliCeco.Text = Solicitud.SoliCecoDesc;

            if (string.IsNullOrEmpty(Solicitud.CiudadOrigen))
            {
                isGasto.IsVisible = true;
                isVuelo.IsVisible = false;
            }
            else
            {
                isGasto.IsVisible = false;
                isVuelo.IsVisible = true;
            }
        }
    }
}