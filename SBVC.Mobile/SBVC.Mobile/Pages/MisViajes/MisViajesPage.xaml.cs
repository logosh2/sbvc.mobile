﻿using Newtonsoft.Json.Linq;
using SBVC.Mobile.Services;
using SBVC.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBVC.Mobile.Pages.MisViajes
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    //[XamlCompilation(XamlCompilationOptions.Skip)]
    public partial class MisViajesPage : ContentPage
    {
        public List<SolicitudesViewModel> lst { get; set; }

        private int _position;
        public int Position { get { return _position; } set { _position = value; OnPropertyChanged(); } }
        public MisViajesPage()
        {
            InitializeComponent();
            this.bgMisViajes.Source = ImageSource.FromResource("SBVC.Mobile.Resources.Images.loginbg1.png");
            this.bgMisViajes.HorizontalOptions = LayoutOptions.FillAndExpand;
            this.bgMisViajes.VerticalOptions = LayoutOptions.FillAndExpand;
            //this.cindicators.UnselectedIndicator ="SBVC.Mobile.Resources.Images.unselected_circle.png";
            //this.cindicators.SelectedIndicator = "SBVC.Mobile.Resources.Images.selected_circle.png";
            cvMisViajes.Margin = new Thickness(30, 80);          
            cindicators.Margin = new Thickness(10, 10);
            Frame.IsVisible = true;
            indicator.IsVisible = true;            
        }

        private void cvMisViajes_Focused(object sender, FocusEventArgs e)
        {

            var obj = cvMisViajes.Item;
            SolicitudDetallePage page = new SolicitudDetallePage();
            page.Solicitud = (SolicitudesViewModel)obj;

            Navigation.PushModalAsync(page);
            return;
        }

        async protected override void OnAppearing()
        {
            base.OnAppearing();
            if (Application.Current.Properties.ContainsKey("user"))
            {
                JObject rss = (JObject)Application.Current.Properties["user"];
                ReportesServices ser = new ReportesServices();
                List<SolicitudesViewModel> misviajes = ser.getMisViajes(rss["UsuaUsua"].ToString());
                // agregarbtn.Margin = new Thickness(80, 20);
                for (int i = 0; i < misviajes.Count; i++)
                {
                    try
                    {
                        if (misviajes[i].CiudadDestino != null)
                        {
                            misviajes[i].IataOrigen = misviajes[i].CiudadOrigen.Split('-')[0];
                            misviajes[i].CiudadOrigen = misviajes[i].CiudadOrigen.Split('-')[1];
                            misviajes[i].IataDestino = misviajes[i].CiudadDestino.Split('-')[0];
                            misviajes[i].CiudadDestino = misviajes[i].CiudadDestino.Split('-')[1];
                            misviajes[i].SoliIsVu = true;
                            misviajes[i].SoliGasAnt = false;
                            misviajes[i].SoliImga = "https://www.aviatur.com/assets/aviatur_assets/img/places/" + misviajes[i].IataDestino + ".jpg";

                        }
                        else
                        {
                            misviajes[i].SoliIsVu = false;
                            misviajes[i].SoliGasAnt = true;
                            misviajes[i].SoliImga = "https://corp.grupoaviatur.com/Content/themes/img/user-bg.png";

                        }
                    }
                    catch (Exception) { }
                }
                lst = misviajes;
                BindingContext = this;           
                Frame.IsVisible = false;
                indicator.IsVisible = false;
            }
            try
            {
                this.Navigation.RemovePage(Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
            }
            catch (Exception ex) { }
         
        }
    }
}