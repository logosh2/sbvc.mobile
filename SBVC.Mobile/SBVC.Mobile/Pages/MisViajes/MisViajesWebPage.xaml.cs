﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBVC.Mobile.Pages.MisViajes
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MisViajesWebPage : ContentPage
    {
        public MisViajesWebPage()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            JObject rss = (JObject)Application.Current.Properties["user"];
            corpUrl.Source = "https://sbvcwebmobile.azurewebsites.net/#/MisViajes";
            try
            {
                this.Navigation.RemovePage(Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
            }
            catch (Exception ex) { }
        }
    }
}