﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBVC.Mobile.Models;
using SBVC.Mobile.Services;
using SBVC.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBVC.Mobile.Pages.Expenses
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExpensesPage   : ContentPage
    {
        public string UsuaId { get; set; }

        public ExpensesPage()
        {
            InitializeComponent();
            JObject user = (JObject)Application.Current.Properties["user"];
            UsuaId = user["UsuaId"].ToString();

        }
        protected override void OnAppearing()
        {
            //actInd.IsRunning = true;
            this.getListRequestExpenses();
            //actInd.IsRunning = false;
        }

        public List<Expense> lst { get; set; }

        protected void getListRequestExpenses()
        {

            ExpenseService serv = new ExpenseService();
            //string result = serv.ListSolicitudesByUser();
            var expenses = serv.ListSolicitudesByUser();

            JArray data = JsonConvert.DeserializeObject<JArray>(expenses);

            List<Expense> expensesList = new List<Expense>();
            for (int i = 0; i < data.Count; i++)
            {
                var expns = data[i].ToObject<JArray>();
                for (int ind = 0; ind < expns.Count; ind++)
                {
                    Expense exp = new Expense();

                    //exp.Id = int.Parse((string)expns[ind]["Id"]);
                    exp.Name = ((string)expns[ind]["Name"]);
                    exp.State = ((string)expns[ind]["State"]);

                    if (((string)expns[ind]["State"]) == "Registrado") { exp.StateColor = "#4caf50"; }
                    if (((string)expns[ind]["State"]) == "Solicitado") { exp.StateColor = "#ff9800"; }
                    if (((string)expns[ind]["State"]) == "Rechazado") { exp.StateColor = "#f44336"; }
                    if (((string)expns[ind]["State"]) == "Aprobado") { exp.StateColor = "#3f51b5"; }
                    if (((string)expns[ind]["State"]) == "Legalizado") { exp.StateColor = "#00bcd4"; }
                    if (((string)expns[ind]["State"]) == "Para Legalizar") { exp.StateColor = "#03a9f4"; }
                    if (((string)expns[ind]["State"]) == "Negado") { exp.StateColor = "#b0bec5"; }

                    exp.Category = ((string)expns[ind]["Category"]);
                    //exp.CreactionDate = DateTime.Parse(((string)expns[ind]["CreactionDate"]));
                    exp.CreactionDateText = ((string)expns[ind]["CreactionDate"]);

                    exp.Text = ((string)expns[ind]["Text"]);
                    exp.Type = ((string)expns[ind]["Type"]);
                    if (((string)expns[ind]["Type"]) == "Expense") { exp.TypeColor = "#f44336"; }
                    if (((string)expns[ind]["Type"]) == "Expenses") { exp.TypeColor = "#f44336"; }
                    if (((string)expns[ind]["Type"]) == "Retainer") { exp.TypeColor = "#3f51b5"; }

                    exp.Coin = ((string)expns[ind]["Coin"]);

                    exp.Amount = long.Parse(((string)expns[ind]["Amount"]).Replace(".00", ""));
                    exp.AmountText = exp.Coin + " $" + exp.Amount.ToString();
                    exp.Text = ((string)expns[ind]["Text"]);

                    //exp.Cc = ((string)expns[ind]["Cc"]);
                    exp.Company = "";

                    string nofactor = null;
                    nofactor = ((string)expns[ind]["Image"]);
                    if (nofactor == "" || nofactor == "null" || nofactor == null || nofactor == "(null)")
                    {
                        exp.ImageShow = ImageSource.FromFile("no_img.png");
                    }
                    else
                    {
                        nofactor = ((string)expns[ind]["Image"]);
                        string img64 = nofactor.Replace("data:image/jpeg;base64,", "").Replace("data:image/png;base64,", "");
                        byte[] Base64Stream = Convert.FromBase64String(img64);
                        exp.ImageShow = ImageSource.FromStream(() => new MemoryStream(Base64Stream));
                    }


                    expensesList.Add(exp);
                }
            }


            ExpensesList.ItemSelected += (object sender, SelectedItemChangedEventArgs e) =>
            {
                Expense item = (Expense)e.SelectedItem;
                Navigation.PushAsync(new ExpenseDetailPage(item));
            };
            ExpensesList.ItemsSource = expensesList;
            BindingContext = this;
        }

        #region Gastos
        #endregion

        private void Add_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new NewExpensePage());
        }
    }
}