﻿using Newtonsoft.Json;
using SBVC.Mobile.Models;
using SBVC.Mobile.Services;
using SBVC.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBVC.Mobile.Pages.Expenses
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewExpensePage : TabbedPage
    {
        public string UsuaId { get; set; }
        string imgJpeg = "data:image/jpeg;base64,";
        string imgPng = "data:image/png;base64,";
        AnticipoViewModel Exp = new AnticipoViewModel();
        GastoViewModel Gasto = new GastoViewModel();
        ExpenseService es = new ExpenseService();
        List<CentroCosto> cecos = new List<CentroCosto>();
        List<User> users = new List<User>();
        List<Empresa> empr = new List<Empresa>();
        List<TipoGasto> category = new List<TipoGasto>();
        List<Coin> coins = new List<Coin>();

        public NewExpensePage()
        {
            InitializeComponent();

            // add pickers

            category.Add(new TipoGasto { TigaId = 1, TigaDesc = "Transporte" });
            category.Add(new TipoGasto { TigaId = 2, TigaDesc = "Alimentación" });
            category.Add(new TipoGasto { TigaId = 3, TigaDesc = "Alojamiento" });
            category.Add(new TipoGasto { TigaId = 4, TigaDesc = "Otros" });
            coins.Add(new Coin { Id = 1, Name = "COP (Peso Colombiano)" });
            coins.Add(new Coin { Id = 2, Name = "USD (Dolar Estadounidense)" });
            coins.Add(new Coin { Id = 3, Name = "EUR (Euro)" });

        }

        protected override void OnAppearing()
        {
            GastTipo.ItemsSource = category;
            AntiCate.ItemsSource = category;
            AntiMone.ItemsSource = coins;
            GastMone.ItemsSource = coins;
            List<object> resultceco = es.ExpensesSearch();
            var c = JsonConvert.SerializeObject(resultceco[0]);
            var u = JsonConvert.SerializeObject(resultceco[2]);
            var e = JsonConvert.SerializeObject(resultceco[1]);
            cecos = JsonConvert.DeserializeObject<List<CentroCosto>>(c);
            users = JsonConvert.DeserializeObject<List<User>>(u);
            empr = JsonConvert.DeserializeObject<List<Empresa>>(e);
            //pickerCecos.SetBinding(Picker.ItemsSourceProperty, "Cecos");
            //pickerCecos.ItemDisplayBinding = new Binding("CecoDesc");
            AntiCeco.ItemsSource = cecos;
            AntiUsua.ItemsSource = users;
            AntiEmpr.ItemsSource = empr;
            GastCeco.ItemsSource = cecos;
            GastCont.ItemsSource = users;
            GastEmpr.ItemsSource = empr;

        }

        //On Selects 
        void OnUsersSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            List<User> ListUsers = new List<User>();
            if (selectedIndex != -1)
            {
                ListUsers = (List<User>)picker.ItemsSource;
                Exp.AntiCont = ListUsers[selectedIndex].UsuaId.ToString();
            }
        }
        void OnCecoSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            List<CentroCosto> ListUsers = new List<CentroCosto>();
            if (selectedIndex != -1)
            {
                ListUsers = (List<CentroCosto>)picker.ItemsSource;
                Exp.AntiCeco = ListUsers[selectedIndex].CecoId;
            }
        }
        void OnEmprSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            List<Empresa> ListUsers = new List<Empresa>();
            if (selectedIndex != -1)
            {
                ListUsers = (List<Empresa>)picker.ItemsSource;
                Exp.AntiEmpr = ListUsers[selectedIndex].EmprId.ToString();
            }
        }
        void OnCateSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            //int selectedIndex = picker.SelectedIndex + 1;
            int selectedIndex = picker.SelectedIndex;

            List<TipoGasto> ListCate = new List<TipoGasto>();
            if (selectedIndex != -1)
            {
                ListCate = (List<TipoGasto>)picker.ItemsSource;
                Exp.AntiCate = ListCate[selectedIndex].TigaId.ToString();
            }
        }
        void OnCoinSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            //int selectedIndex = picker.SelectedIndex + 1;
            int selectedIndex = picker.SelectedIndex;

            List<Coin> ListCoin = new List<Coin>();
            if (selectedIndex != -1)
            {
                ListCoin = (List<Coin>)picker.ItemsSource;
                Exp.AntiMone = ListCoin[selectedIndex].Id.ToString();
            }
        }
        void DatePicker_OnDateSelected(object sender, DateChangedEventArgs e)
        {
            //DisplayAlert("Fecha", e.NewDate.Date.ToString("dd/MM/yyyy"), "OK");
            Exp.AntiFeAu = e.NewDate.Date.ToString("yyyy-MM-dd").ToString();
        }
        void OnButtonBtnAnticipoClicked(object sender, EventArgs e)
        {
            ExpenseService serv = new ExpenseService();
            DateTime date = new DateTime();
            //Exp.AntiCate = "4";
            Exp.AntiSoli = "";
            Exp.AntiMont = AntiMont.Text;
            Exp.AntiEsta = "Solicitado";
            //Exp.AntiTeAu = AntiTeAu.ToString();
            if (Exp.AntiFeAu == null)
            {
                "Debes seleccionar una fecha".ToToast(ToastNotificationType.Error);
                return;
            }
            Exp.AntiObse = AntiObse.Text;
            //Exp.AntiUsua = "3405";
            //Exp.AntiMone = AntiMone.SelectedIndex.ToString();
            //Exp.AntiCeco = 1645;

            AnticipoViewModel anticipo = serv.CreateAnticipo(Exp);
            DisplayAlert("Información", $"Anticipo creado ID: {anticipo.AntiId}", "Aceptar");
        }


        void OnUsersGastSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            List<User> ListUsers = new List<User>();
            if (selectedIndex != -1)
            {
                ListUsers = (List<User>)picker.ItemsSource;
                Gasto.GastCont = ListUsers[selectedIndex].UsuaId.ToString();
            }
        }
        void OnCecoGastSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            List<CentroCosto> ListCecos = new List<CentroCosto>();
            if (selectedIndex != -1)
            {
                ListCecos = (List<CentroCosto>)picker.ItemsSource;
                Gasto.GastCeco = ListCecos[selectedIndex].CecoId;
            }
        }
        void OnEmprGastSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            List<Empresa> ListEmpr = new List<Empresa>();
            if (selectedIndex != -1)
            {
                ListEmpr = (List<Empresa>)picker.ItemsSource;
                Gasto.GastEmpr = ListEmpr[selectedIndex].EmprId;
            }
        }
        void OnTypeGastSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            List<TipoGasto> ListTypeG = new List<TipoGasto>();
            if (selectedIndex != -1)
            {
                ListTypeG = (List<TipoGasto>)picker.ItemsSource;
                Gasto.GastTipo = ListTypeG[selectedIndex].TigaId;
            }
        }
        void OnButtonBtnGastoClicked(object sender, EventArgs e)
        {
            string result = "Su Gasto fue creado y enviado";
            ExpenseService serv = new ExpenseService();

            Gasto.GastObse = GastObse.Text;
            Gasto.GastValo = Convert.ToDouble(GastValo.Text);
            Gasto.GastUsua = UsuaId;

            if (string.IsNullOrEmpty(Gasto.GastCont))
            {
                "Debes seleccionar pasajero".ToToast(ToastNotificationType.Error);
                return;
            }
            if (Gasto.GastCeco == 0)
            {
                "Debes seleccionar centro de costos".ToToast(ToastNotificationType.Error);
                return;
            }
            if (Gasto.GastEmpr == 0)
            {
                "Debes seleccionar nit de empresa".ToToast(ToastNotificationType.Error);
                return;
            }
            if (Gasto.GastTipo == 0)
            {
                "Debes seleccionar categoría".ToToast(ToastNotificationType.Error);
                return;
            }
            if (string.IsNullOrEmpty(Gasto.GastObse))
            {
                "Debes agregar una descripción".ToToast(ToastNotificationType.Error);
                return;
            }
            if (string.IsNullOrEmpty(Gasto.GastFech))
            {
                "Debes seleccionar una fecha".ToToast(ToastNotificationType.Error);
                return;
            }
            //if (!Gasto.GastFech.Contains("-"))
            //{
            //    var DateSel = Gasto.GastFech.Split(' ')[0];
            //    var m = DateSel.Split('/')[0];
            //    var d = DateSel.Split('/')[1];
            //    var a = DateSel.Split('/')[2];
            //    Gasto.GastFech = $"{a}-{m}-{d}";
            //}
            if (Gasto.GastValo == 0)
            {
                "Debes agregar un monto".ToToast(ToastNotificationType.Error);
                return;
            }
            if (Gasto.GastMone == 0)
            {
                "Debes seleccionar tipo de moneda".ToToast(ToastNotificationType.Error);
                return;
            }

            string gasto = serv.CreateGasto(Gasto);
            if (!gasto.Contains("GastId"))
            {
                result = gasto;
            }
            //DisplayAlert("Información", "Gasto(s) creado(s)", "Aceptar");
            result.ToToast(ToastNotificationType.Success);
        }

        void OnMoneGastSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            List<Coin> ListCoin = new List<Coin>();
            if (selectedIndex != -1)
            {
                ListCoin = (List<Coin>)picker.ItemsSource;
                Gasto.GastMone = ListCoin[selectedIndex].Id;
            }
        }
        void OnSwReemChanged(object sender, EventArgs e)
        {
            var sw = (Switch)sender;
            if (sw.IsToggled)
            {
                Gasto.GastReem = 1;
            }
            else
            {
                Gasto.GastReem = 2;
            }

        }
        void DatePickerGas_OnDateSelected(object sender, DateChangedEventArgs e)
        {
            //DisplayAlert("Fecha", e.NewDate.ToString(), "OK");
            Gasto.GastFech = e.NewDate.Date.ToString("yyyy-MM-dd").ToString();
        }
        async void OnButtonBtnImageClicked(object sender, EventArgs e)
        {
            string auxPath = string.Empty;
            string auxType = string.Empty;
            var photo = await Plugin.Media.CrossMedia.Current.PickPhotoAsync();

            if (photo != null)
            {
                ImageAdj.Source = ImageSource.FromStream(() => { return photo.GetStream(); });
                var stream2 = photo.GetStream();
                var bytes = new byte[stream2.Length];
                await stream2.ReadAsync(bytes, 0, (int)stream2.Length);
                string base64 = System.Convert.ToBase64String(bytes);
                if (Device.RuntimePlatform == Device.Android)
                {
                    try
                    {
                        auxPath = photo.Path.Split('/')[10];
                        auxType = auxPath.Split('.')[1];
                        switch (auxType)
                        {
                            case "jpg":
                                Gasto.GastImag = imgJpeg + base64;
                                break;
                            case "png":
                                Gasto.GastImag = imgPng + base64;
                                break;
                            default:
                                Gasto.GastImag = base64;
                                break;
                        }
                    }
                    catch (Exception) { }
                }
                else { Gasto.GastImag = base64; }


            }
            //Stream stream = await DependencyService.Get<IPicture>().GetImageStreamAsync();
            //if (stream != null)
            //{
            //    Image image = new Image
            //    {
            //        Source = ImageSource.FromStream(() => stream),
            //        BackgroundColor = Color.Gray
            //    };
            //    ImageAdj.Source = ImageSource.FromStream(() => stream);
            //}
            //else
            //{
            //    BtnImage.IsEnabled = true;
            //}
        }
    }
}