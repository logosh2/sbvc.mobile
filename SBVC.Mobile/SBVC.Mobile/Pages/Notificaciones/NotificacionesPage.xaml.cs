﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBVC.Mobile.Pages.Notificaciones
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotificacionesPage : ContentPage
    {

        public NotificacionesPage()
        {
            InitializeComponent();
            this.Padding = new Thickness(10, 10,10, 10);

            List<Notificaciones> lista = new List<Notificaciones>();
            listaNots.ItemsSource = null;

            lblNoti.Margin = new Thickness(30, 10, 30, 5);
            listaNots.Margin = new Thickness(10, 10);
            var closepage = new ToolbarItem
            {
                Icon = "fa-times",
                Command = new Command(ClosePage)
            };

            this.ToolbarItems.Add(closepage);
            this.WidthRequest = 250;

            for (int i = 0; i < App.Notificaciones.Count; i++)
            {
                string titulo = App.Notificaciones[i].Split('|')[0];
                string texto = App.Notificaciones[i].Split('|')[1];
                string fecha = App.Notificaciones[i].Split('|')[2];

                if (string.IsNullOrEmpty(titulo))
                {
                    if (texto.Length >= 20)
                        titulo = texto.Substring(0, 20);
                    else
                        titulo = texto;
                }
                if (titulo.Length >= 20)
                    lista.Add(new Notificaciones { Mensaje = texto, Descripcion = titulo.Substring(0, 20), Logo = "", IsVisible = false,Fecha= fecha });
                else
                    lista.Add(new Notificaciones { Mensaje = texto, Descripcion = titulo, Logo = "", IsVisible = false,Fecha= fecha });
            }

            listaNots.ItemsSource = lista;           
        }
        
        public static Image ImageFromBase64(string base64picture)
        {
            byte[] imageBytes = Convert.FromBase64String(base64picture);
            return new Image { Source = ImageSource.FromStream(() => new MemoryStream(imageBytes)) };
        }

        private void listaNots_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var product = e.Item as Notificaciones;
            DisplayAlert(product.Descripcion, product.Mensaje, "OK");
        }

        private void ClosePage()
        {
            this.Navigation.PushModalAsync(new NotificacionesPage());
        }
    }

    public class Notificaciones
    {
        public string Mensaje { get; set; }
        public string Descripcion { get; set; }
        public string Fecha { get; set; }
        public string Logo { get; set; }
        public bool IsVisible { get; set; }
    }
}