﻿using SBVC.Mobile.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBVC.Mobile.Pages.Login
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PassRecoveryPage : ContentPage
    {
        public PassRecoveryPage()
        {
            InitializeComponent();
            this.bgLogin.Source = ImageSource.FromResource("SBVC.Mobile.Resources.Images.loginbg1.png");
            this.bgLogin.HorizontalOptions = LayoutOptions.FillAndExpand;
            this.bgLogin.VerticalOptions = LayoutOptions.FillAndExpand;
            this.logoAviatur.Source = ImageSource.FromResource("SBVC.Mobile.Resources.Images.Logos.logoAviatur.png");
            this.logoAviatur.Margin = new Thickness(40, 30);

            this.buttonRecStyle.Margin = new Thickness(80, 5);


            Username.Margin = new Thickness(80, 10);
            buttonRecStyle.Margin = new Thickness(80, 10);
            indicator.IsVisible = false;
            indicator.IsRunning = false;
        }
        private async void buttonRecStyle_Clicked(object sender, EventArgs e)
        {
            indicator.IsVisible = true;
            indicator.IsRunning = true;

            if (this.Username.Text == null)
            {
                "Por favor ingresar su correo electronico".ToToast(ToastNotificationType.Error);
                indicator.IsVisible = false;
                indicator.IsRunning = false;
                return;
            }
            LoginServices servicioLogin = new LoginServices();
            string respuesta = servicioLogin.PasswordRecovery(this.Username.Text);
            respuesta.ToToast(ToastNotificationType.Success);
            indicator.IsVisible = false;
            indicator.IsRunning = false;
            await Task.Delay(2000);
            await Navigation.PopModalAsync();
            return;
        }
    }
}