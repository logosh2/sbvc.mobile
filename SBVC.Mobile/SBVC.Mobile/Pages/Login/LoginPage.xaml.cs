﻿using Com.OneSignal;
using Com.OneSignal.Abstractions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBVC.Mobile.Pages.MenuView;
using SBVC.Mobile.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBVC.Mobile.Pages.Login
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            /*
          * Diseño login
          * */
            this.bgLogin.Source = ImageSource.FromResource("SBVC.Mobile.Resources.Images.loginbg1.png");
            this.bgLogin.HorizontalOptions = LayoutOptions.FillAndExpand;
            this.bgLogin.VerticalOptions = LayoutOptions.FillAndExpand;
            this.logoAviatur.Source = ImageSource.FromResource("SBVC.Mobile.Resources.Images.Logos.logoAviatur.png");
            this.logoAviatur.Margin = new Thickness(40, 30);

            this.buttonLoginStyle.Margin = new Thickness(80, 5);
            this.btnPassRecovery.Margin = new Thickness(80, 5);
            this.btnPassRecovery.VerticalOptions = LayoutOptions.Center;

            this.Username.Margin = new Thickness(40, 5);
            this.Password.Margin = new Thickness(40, 5);
            this.BindingContext = this;
            // passrecovery label tap event
            var forgetPassword_tap = new TapGestureRecognizer();
            forgetPassword_tap.Tapped += (s, e) =>
            {
                Navigation.PushModalAsync(new PassRecoveryPage());
            };
            this.btnPassRecovery.GestureRecognizers.Add(forgetPassword_tap);
            this.BindingContext = this;
        }

        void EntryKeyboardHandle_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            this.logoAviatur.IsVisible = false;
            this.BindingContext = this;
        }
        public void ShowPass(object sender, EventArgs args)
        {
            Password.IsPassword = Password.IsPassword ? false : true;
            if (Password.IsPassword)
            {
                this.eye.Icon = "fa-eye-slash";
            }
            else { this.eye.Icon = "fa-eye"; }
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                this.Navigation.RemovePage(Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
            }
            catch (Exception ex) { }
        }
        public void OnButtonLoginClicked(object sender, EventArgs e)
        {
            Frame.IsVisible = true;
            indicator.IsVisible = true;
            this.BindingContext = this;
            if (this.Username.Text == null || this.Password.Text == null)
            {
                "Las cedenciales no coinciden por favor intentar nuevamente".ToToast(ToastNotificationType.Error);
                Frame.IsVisible = false;
                indicator.IsVisible = false;
                this.BindingContext = this;
                return;
            }
            LoginServices servicioLogin = new LoginServices();
            string respuesta = servicioLogin.Auth(this.Username.Text, this.Password.Text);
            JObject rss = JObject.Parse(respuesta);
            if (((string)rss["UsuaId"]).ToUpper().Equals("0"))
            {
                "Las cedenciales no coinciden por favor intentar nuevamente".ToToast(ToastNotificationType.Error);
                Frame.IsVisible = false;
                indicator.IsVisible = false;
                this.BindingContext = this;
                return;
            }
            else
            {
                if (Application.Current.Properties.ContainsKey("user"))
                {
                    var username = Application.Current.Properties["user"] as string;
                }
                else
                {
                    Application.Current.Properties.Add("user", rss);
                }
                BuscadorServices bs = new BuscadorServices();
                try
                {
                    OneSignal.Current.StartInit("a7b2259a-1fa5-41ae-bc95-896d5911ed9b").HandleNotificationOpened(
                        (result) =>
                        {                            
                            Debug.WriteLine("HandleNotificationOpened: {0}", result.notification.payload.title +"|"+ result.notification.payload.body);
                            App.Notificaciones.Add(result.notification.payload.title + "|" + result.notification.payload.body + "|"+DateTime.Now.ToString("dd-MM-yyyy HH:mm"));
                            result.notification.payload.body.ToToast(ToastNotificationType.Error);                          
                        })
                          .HandleNotificationReceived((notification) =>
                          {
                              Debug.WriteLine("HandleNotificationReceived: {0}", notification.payload.title+"|"+notification.payload.body);
                              App.Notificaciones.Add(notification.payload.title + "|" + notification.payload.body + "|" + DateTime.Now.ToString("dd-MM-yyyy HH:mm"));
                              notification.payload.body.ToToast(ToastNotificationType.Error);                              
                          }
                        );
                    OneSignal.Current.StartInit("a7b2259a-1fa5-41ae-bc95-896d5911ed9b").HandleNotificationReceived(HandleNotificationReceived).EndInit();
                    OneSignal.Current.SendTags(new Dictionary<string, string>() { { "email", ((string)rss["UsuaUsua"]).ToUpper() } });
                }
                catch (Exception) { }

                var perfil = "";
                try
                {
                    perfil = bs.PerfilBuscador(((string)rss["UsuaId"]));
                    string jsonUsuario = perfil.Substring(perfil.IndexOf("UsuaId") - 2, perfil.IndexOf("null}") + 4);
                    string jsonParametrizacion = perfil.Substring(perfil.IndexOf("PaemId") - 2, perfil.IndexOf("\"PaEmMaSi\":null}") - perfil.IndexOf("PaemId") + 18);
                    JObject perfilJson = JObject.Parse((string)jsonUsuario);
                    JObject paraJson = JObject.Parse((string)jsonParametrizacion);
                    var menu = rss["Usuarios_Roles"][0]["Roles"]["Roles_Permisos"];
                    Application.Current.Properties.Add("perfil", perfil);
                    Application.Current.Properties.Add("menu", menu);
                    Application.Current.Properties.Add("param", paraJson);
                }
                catch (Exception ex)
                {
                    dynamic jsonDe = JsonConvert.DeserializeObject(perfil);
                }
                Frame.IsVisible = false;
                indicator.IsVisible = false;
                Navigation.PushAsync(new MenuPage());
            }
        }

        private static void HandleNotificationReceived(OSNotification notification)
        {
            OSNotificationPayload payload = notification.payload;
            string message = payload.body;

            // print("GameControllerExample:HandleNotificationReceived: " + message);
            //print("displayType: " + notification.displayType);
            //extraMessage = "Notification received with text: " + message;
        }

        void OnButtonPassRecClicked(object sender, EventArgs e)
        {

        }
    }
}