﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBVC.Mobile.Pages.Documentacion
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DocumentacionPage : ContentPage
    {
        public DocumentacionPage()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            JObject rss = (JObject)Application.Current.Properties["user"];
            corpUrl.Source = "https://sbvcwebmobile.azurewebsites.net/#/Documentos";
            try
            {
                this.Navigation.RemovePage(Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
            }
            catch (Exception ex) { }
        }
    }
}