﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBVC.Mobile.Pages.MisPendientes
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MisPendientesPage : ContentPage
    {
        public MisPendientesPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            JObject rss = (JObject)Application.Current.Properties["user"];
            corpUrl.Source = "https://corp.grupoaviatur.com/#/MisPendientes";
            try
            {
                this.Navigation.RemovePage(Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
            }
            catch (Exception ex) { }
        }
    }
}