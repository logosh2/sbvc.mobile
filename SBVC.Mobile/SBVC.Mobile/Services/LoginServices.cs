﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBVC.Mobile.Services
{
    public class LoginServices
    {
        public string Auth(string username, string password)
        {
            string json = string.Empty;
            string postData = "{'UsuaUsua':'" + username.ToUpper().Trim() + "','UsuaPwd':'" + password + "'}";
            RequestProvider rp = new RequestProvider();
            json = rp.GetAsync<string>(RutasServicios.URLCORP + "/RestService/Auth/" + username.ToUpper().Trim() + "/" + password).Result;
            return json;
        }
        public string PasswordRecovery(string username)
        {
            string json = string.Empty;          
            RequestProvider rp = new RequestProvider();
            json = rp.GetAsync<string>(RutasServicios.URLCORP + "/RestService/PassRec/" + username.ToUpper().Trim()).Result;            
            return json;
        }


    }
}
