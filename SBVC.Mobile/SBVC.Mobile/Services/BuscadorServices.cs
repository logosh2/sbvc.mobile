﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBVC.Mobile.Services
{
    public class BuscadorServices
    {
        RequestProvider rp = new RequestProvider();
        public string PerfilBuscador(string username)
        {
            string json = string.Empty;
            RequestProvider rp = new RequestProvider();
            json = rp.GetAsync<string>(RutasServicios.URLCORP + "/RestService/FlightSearch/" + username.ToUpper().Trim()).Result;
            return json;
        }

        public string AirSearch(string usuausua, string module, string command, string trips, string paxes, string airline, string class_, string escalas, string multi, string empresa, string maleta)
        {
            string json = "{\"request\":[ {UsuaUsua:'" + usuausua + "',Module:'" + module + "',Command:'" + command + "'},{'Trips':" + trips + " ,'Adults':'" + paxes + "','Airline':{'Value':'" + airline + "'},'Class':'" + class_ + "','Scale':'" + escalas + "','Multi':'" + multi + "','Username':'" + usuausua + "','maxresponse':'10','FlightType':'N','Preference':'N-A','Bag':'" + maleta + "','CorreoPax':'null','CompanyId':'" + empresa + "','UrlAvail':'/Disponibilidad/origen/BOG/destino/CTG/numeropax/1/fechasalida/2018-01-05/fecharegreso/2019-01-10/aerolinea/N-A/clase/N-A/multi/N-A/con/N-A/hotel/0/auto/0/conxPref/N-A/tipovuelo/N/maleta/N-A'} ]}";

            RequestProvider rp = new RequestProvider();
            json = rp.PostAsyncString<string>(RutasServicios.URLCORP + "/RestService/Air", json).Result;
            return json;
        }

        public string AirDetail(string usuausua, string module, string command, string trans, string quote, string correlation, string segmentos, string gds, string multi, string empresa, string aerolinea, string ciudadDestino)
        {
            string json = "{\"request\":\"[{ 'UsuaUsua':'" + usuausua + "','Module':'" + module + "','Command':'" + command + "'},{ 'RQDetail':{ 'Transaction':'" + trans + "','Quote':'" + quote + "','Correlation':'" + correlation + "','Segment':'" + segmentos + "','GDS':'" + gds + "'},'RphGoing':'0','RphReturn':'0','Airline':{ 'Value':'" + aerolinea + "'},'CityTo':{ 'Value':'" + ciudadDestino + "'},'UrlAvail': '/Disponibilidad/origen/BOG/destino/CTG/numeropax/1/fechasalida/2017-12-29/fecharegreso/2018-01-05/aerolinea/N-A/clase/N-A/multi/N-A/con/N-A/hotel/0/auto/0/conxPref/N-A/tipovuelo/N/maleta/N-A'}]\"}";

            RequestProvider rp = new RequestProvider();
            json = rp.PostAsyncString<string>(RutasServicios.URLCORP + "/RestService/Air", json).Result;
            return json;
        }
        public string AirXS(string origen, string destino)
        {
            string json = "{\"PriceXplorer_ExtremeSearch\": {\"@xmlns\": \"http://xml.amadeus.com/FAESXQ_12_2_1A\", \"itineraryGrp\": { \"itineraryInfo\": { \"origin\": \"" + origen + "\",\"destination\":\"" + destino + "\" }  }, \"travelDates\": { " +
            "\"dateAndTimeDetails\": [{\"qualifier\": \"S\",\"date\": \"010718\"}, {\"qualifier\": \"E\",\"date\": \"300918\"" +
        "}]},\"stayDuration\": {\"nbOfUnitsInfo\": {\"quantityDetails\": {\"numberOfUnit\": \"2\",\"unitQualifier\": \"DAY\"" +
        "}}},\"attributeInfo\":{ \"attributeFunction\":\"GRP\",\"attributeDetails\":[{\"attributeType\":\"DES\"},{\"attributeType\":\"WEEK\"},{\"attributeType\":\"DAY\"}]}," +
    "\"departureDays\": {\"daySelection\": { \"dayOfWeek\": \"1234567\" }, \"selectionInfo\": {\"selectionDetails\": { \"option\": \"O\" } }},\"officeIdInfo\": {" +
      "\"officeId\": {\"originIdentification\": { \"inHouseIdentification1\": \"BOGVU28AT\" } } }}}";
            RequestProvider rp = new RequestProvider();
            json = rp.PostAsyncString<string>(RutasServicios.URLEXTREMESEARCH, json).Result;
            return json;
        }
    }
}
