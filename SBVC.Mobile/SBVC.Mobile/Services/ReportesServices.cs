﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBVC.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBVC.Mobile.Services
{
    public class ReportesServices
    {
        RequestProvider rp = new RequestProvider();
        public List<SolicitudesViewModel> getMisViajes(string usuausua)
        {
            string json = "{\"request\":\"[{ 'UsuaUsua': '" + usuausua+ "', 'Module': 'Report', 'Method': 'ListSolicitudes'}]\"}";
            RequestProvider rp = new RequestProvider();
            try {
                json = rp.PostAsyncString<string>(RutasServicios.URLCORP + "/RestService/Report", json).Result;
            }
            catch (Exception ex) {
                json = ex.Message;
            }            
            var lista= JsonConvert.DeserializeObject<List<SolicitudesViewModel>>(json);
            List<SolicitudesViewModel> solicitudes = new List<SolicitudesViewModel>();            
            return lista;

        }
    }
}
