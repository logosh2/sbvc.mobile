﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBVC.Mobile.Models;
using SBVC.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SBVC.Mobile.Services
{
    public class ExpenseService
    {
        public string UsuaMail { get; set; }
        public string UsuaId { get; set; }

        public ExpenseService()
        {
            JObject user = (JObject)Application.Current.Properties["user"];
            UsuaMail = user["UsuaUsua"].ToString();
            UsuaId= user["UsuaId"].ToString();
        }

        #region CreateAnticipo
        //Método para crear un anticipo
        public AnticipoViewModel CreateAnticipo(AnticipoViewModel Expense)
        {
            string data = JsonConvert.SerializeObject(Expense);
            string json = "{\"request\":\"[{'UsuaUsua':'" + UsuaMail + "','Module':'Expenses','Method':'CreateAnticipos'},{'AntiCate':'" + Expense.AntiCate + 
                "','AntiCeco':'" + Expense.AntiCeco + "'," + "'AntiCont':" + Expense.AntiCont + ",'AntiEmpr':'" + Expense.AntiEmpr + "','AntiEsta':'" + Expense.AntiEsta +
                "','AntiFeAu':'" + Expense.AntiFeAu + "','AntiMone':'" + Expense.AntiMone + "','AntiMont':'" + Expense.AntiMont + "','AntiObse':'" + Expense.AntiObse +
                "'," + "'AntiUsua':'" + UsuaId + "','Booking':null}]\"}";
            //string json = "{\"request\":\"[{'UsuaUsua':'Demoapro@yahoo.com','Module':'Expenses','Method':'CreateAnticipos'},"+ data +"]\"}";
            RequestProvider rp = new RequestProvider();
            try
            {
                json = rp.PostAsyncString<string>(RutasServicios.URLCORP + "/RestService/Expenses", json).Result;
            }
            catch (Exception ex)
            {
                json = ex.Message;
            }
            var rs = JsonConvert.DeserializeObject<AnticipoViewModel>(json);
            return rs;
        }
        #endregion

        #region CreateGasto
        //Método para crear un gasto
        public string CreateGasto(GastoViewModel gasto)
        {   
            string json = "{\"request\":\"[{'UsuaUsua':'" + UsuaMail + "','Module':'Expenses','Method':'CreateGasto'},[{'GastCeco':'" + gasto.GastCeco + "','GastCont':'" + gasto.GastCont + "','GastEmpr':'" + gasto.GastEmpr + "','GastFech':'" + gasto.GastFech + "','GastImag':'" + gasto.GastImag + "','GastMone':'" + gasto.GastMone + "','GastObse':'" + gasto.GastObse + "','GastReem':'" + gasto.GastReem + "','GastTipo':'" + gasto.GastTipo + "','GastUsua':'" + gasto.GastUsua + "','GastValo':'" + gasto.GastValo + "','GastViaj':'" + gasto.GastViaj + "','GastLugar':'" + gasto.GastLugar + "'}]]\"}";
            RequestProvider rp = new RequestProvider();
            try
            {
                json = rp.PostAsyncString<string>(RutasServicios.URLCORP + "/RestService/Expenses", json).Result;
            }
            catch (Exception ex)
            {
                json = ex.Message;
            }
            //var rs = JsonConvert.DeserializeObject<List<GastoViewModel>>(json);
            //var rs = JsonConvert.DeserializeObject<GastoViewModel>(json);
            return json;
        }
        #endregion

        #region ExpensesSearch
        public List<object> ExpensesSearch()
        {   
            string json = "{\"request\":\"[{'UsuaUsua':'" + UsuaMail + "','Module':'Expenses','Method':'ExpensesSearch'},{'Passenger':{'Rol':{'Id':192}}}]\"}";
            RequestProvider rp = new RequestProvider();
            try
            {
                json = rp.PostAsyncString<string>(RutasServicios.URLCORP + "/RestService/Expenses", json).Result;
            }
            catch (Exception ex)
            {
                json = ex.Message;
            }

            var deserializedItems = JsonConvert.DeserializeObject<List<object>>(json);

            return deserializedItems;
        }
        #endregion

        #region GetSolicitudFull
        public string GetSolicitudFull()
        {
            string json = "{\"request\":\"[{'UsuaUsua':'" + UsuaMail + "','Module':'Expenses','Method':'getSolicitudFull'},{'Id':'SA.96'}]\"}";
            RequestProvider rp = new RequestProvider();
            try
            {
                json = rp.PostAsyncString<string>(RutasServicios.URLCORP + "/RestService/Expenses", json).Result;
            }
            catch (Exception ex)
            {
                json = ex.Message;
            }
            return json;
        }
        #endregion

        #region ListSolicitudesByUser
        public string ListSolicitudesByUser()
        {
            string json = "{\"request\":\"[{'UsuaUsua':'" + UsuaMail + "','Module':'Expenses','Method':'ListSolicitudesByUser'}]\"}";
            
            RequestProvider rp = new RequestProvider();
            try
            {
                json = rp.PostAsyncString<string>(RutasServicios.URLCORP + "/RestService/Expenses", json).Result;
            }
            catch (Exception ex)
            {
                json = ex.Message;
            }
            return json;
        }
        #endregion
    }
}
