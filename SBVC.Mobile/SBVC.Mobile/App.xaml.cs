﻿using Com.OneSignal;
using SBVC.Mobile.Pages.MenuView;
using SBVC.Mobile.Pages.Splash;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace SBVC.Mobile
{
    public partial class App : Application
    {

        public static List<string> Notificaciones
        {
            get;
            set;
        } = new List<string>();
        public App()
        {
            InitializeComponent();
            OneSignal.Current.StartInit("a7b2259a-1fa5-41ae-bc95-896d5911ed9b").EndInit();
            if (Application.Current.Properties.ContainsKey("user"))
            {
                NavigationPage page = new NavigationPage(new MenuPage());
                page.BarBackgroundColor = Color.Transparent;
                page.BarTextColor = Color.White;
                //page.Icon=
                MainPage = page;
            }
            else
            {
                NavigationPage page = new NavigationPage(new Splash());
                page.BarBackgroundColor = Color.Transparent;
                page.BarTextColor = Color.White;
                //page.Icon=
                MainPage = page;
            }
            //MainPage = new SBVC.Mobile.MainPage();
        }

        protected override void OnStart()
        {
            OneSignal.Current.RegisterForPushNotifications();
            if (Application.Current.Properties.ContainsKey("user"))
            {
                NavigationPage page = new NavigationPage(new MenuPage());
                page.BarBackgroundColor = Color.Transparent;
                page.BarTextColor = Color.White;
                //page.Icon=
                MainPage = page;
            }
            else
            {
                NavigationPage page = new NavigationPage(new Splash());
                page.BarBackgroundColor = Color.Transparent;
                page.BarTextColor = Color.White;
                //page.Icon=
                MainPage = page;
            }
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            if (Application.Current.Properties.ContainsKey("user"))
            {
                NavigationPage page = new NavigationPage(new MenuPage());
                page.BarBackgroundColor = Color.Transparent;
                page.BarTextColor = Color.White;
                //page.Icon=
                MainPage = page;
            }
            else
            {
                NavigationPage page = new NavigationPage(new Splash());
                page.BarBackgroundColor = Color.Transparent;
                page.BarTextColor = Color.White;
                //page.Icon=
                MainPage = page;
            }
        }
    }
}
