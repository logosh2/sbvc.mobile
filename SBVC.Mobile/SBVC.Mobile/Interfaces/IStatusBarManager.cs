﻿using System;
using Xamarin.Forms;

namespace SBVC.Mobile
{
	public interface IStatusBarManager
	{
		void SetColor(string color);
		void SetColor(Color color);
	}
}
