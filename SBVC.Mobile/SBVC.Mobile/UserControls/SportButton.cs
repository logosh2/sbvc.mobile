﻿using System;
using Xamarin.Forms;
using System.Diagnostics;

namespace SBVC.Mobile
{
	public class SBVCButton : Button
	{
		public SBVCButton() : base()
		{
			const int _animationTime = 100;
			Clicked += async(sender, e) =>
			{
				var btn = (SBVCButton)sender;
				await btn.ScaleTo(1.2, _animationTime);
				btn.ScaleTo(1, _animationTime);
			};
		}
	}
}