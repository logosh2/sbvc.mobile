﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SBVC.Mobile.UserControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MisViajesListView : ListView
    {
        public MisViajesListView() : base(ListViewCachingStrategy.RecycleElement)
        {
            InitializeComponent();
        }
    }
}