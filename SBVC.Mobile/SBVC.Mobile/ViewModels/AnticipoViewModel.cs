﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBVC.Mobile.ViewModels
{
    public class AnticipoViewModel
    {
        public int AntiId { get; set; }
        public string AntiUsua { get; set; }
        public string AntiEmpr { get; set; }
        public string AntiSoli { get; set; }
        public string AntiMont { get; set; }
        public string AntiEsta { get; set; }
        public string AntiTeAu { get; set; }
        //public Nullable<System.DateTime> AntiFeAu { get; set; }
        public string AntiFeAu { get; set; }
        public string AntiObse { get; set; }
        public string AntiCont { get; set; }
        public string AntiMone { get; set; }
        public string AntiCate { get; set; }
        public Nullable<int> AntiCeco { get; set; }
    }
}
