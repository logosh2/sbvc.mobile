﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBVC.Mobile.ViewModels
{
    public class SolicitudesViewModel
    {
        public int SoliId { get; set; }
        public Nullable<System.DateTime> SoliFech { get; set; }
        public string SoliSoli { get; set; }
        public string SoliCont { get; set; }
        public string SoliEmail { get; set; }
        public Nullable<int> SoliCeco { get; set; }
        public Nullable<int> SoliDepto { get; set; }
        public string SoliTeco { get; set; }
        public string SoliDico { get; set; }
        public string SoliObse { get; set; }
        public Nullable<int> SoliTivi { get; set; }
        public string SoliDocu { get; set; }
        public string SoliCelu { get; set; }
        public string SoliTaas { get; set; }
        public string SoliMoti { get; set; }
        public Nullable<int> SoliEsta { get; set; }
        public Nullable<int> SoliEmpr { get; set; }
        public Nullable<System.DateTime> SoliFeco { get; set; }
        public Nullable<System.DateTime> SoliFeau { get; set; }
        public Nullable<System.DateTime> SoliFeem { get; set; }
        public string SoliRese { get; set; }
        public string SoliPoli { get; set; }
        public Nullable<int> SoliCoti { get; set; }
        public string SoliEmcc { get; set; }
        public Nullable<int> SoliViaj { get; set; }
        public Nullable<int> SoliAuto { get; set; }
        public Nullable<System.DateTime> SoliFeroau { get; set; }
        public Nullable<int> SoliCoau { get; set; }
        public string SoliJust { get; set; }
        public Nullable<System.DateTime> SoliFeve { get; set; }
        public string SOLINSAP { get; set; }
        public byte[] SoliAdju { get; set; }
        public string SoliAdty { get; set; }
        public string SoliAdno { get; set; }
        public Nullable<int> SoliDivi { get; set; }
        public string SoliCoju { get; set; }
        public string SoliTanu { get; set; }
        public Nullable<int> SoliComp { get; set; }
        public Nullable<int> SoliExpe { get; set; }
        public string SoliAero { get; set; }
        public string SoliClass { get; set; }
        public Nullable<int> SoliNupa { get; set; }
        public string SoliSolaso { get; set; }
        public Nullable<int> SoliRetra { get; set; }
        public string SoliRetraI { get; set; }
        public string SoliRetraV { get; set; }
        public string SoliTrans { get; set; }
        public string SoliCecoDesc { get; set; }
        public string SoliUnidDesc { get; set; }
        public string SoliDiviDesc { get; set; }
        public decimal SoliVaVu { get; set; }
        public decimal SoliVaVuBase { get; set; }
        public decimal SoliVaVuIva { get; set; }
        public decimal SoliVaVuImpuestos { get; set; }

        public decimal SoliVaHo { get; set; }
        public decimal SoliVaAn { get; set; }
        public decimal SoliVaGa { get; set; }

        public decimal SoliVaEx { get; set; }

        public int SoliTiHo { get; set; }
        public string SoliCorreoAuto { get; set; }
        public string justiRechazo { get; set; }
        public string itinerario { get; set; }
        public string nombrePasajeros { get; set; }
        public string CiudadOrigen { get; set; }
        public string FechaOrigen { get; set; }
        public string HoraOrigen { get; set; }
        public string CiudadDestino { get; set; }
        public string FechaDestino { get; set; }
        public string HoraDestino { get; set; }
        public string cantTiempo { get; set; }
        public string NomEmpresa { get; set; }
        public string nombreAuto { get; set; }
        public string vueloIda { get; set; }
        public string vueloRegreso { get; set; }
        public string NomEstado { get; set; }
        public string CogerSoli { get; set; }
        public string NomHotel { get; set; }
        public string SoliOtros { get; set; }
        public string SoliImga { get; set; }
        public string IataOrigen { get; set; }
        public string IataDestino { get; set; }
        public bool SoliGasAnt { get; set; }
        public bool SoliIsVu { get; set; }
    }
}
