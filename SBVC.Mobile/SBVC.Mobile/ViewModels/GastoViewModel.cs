﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBVC.Mobile.ViewModels
{
    public class GastoViewModel
    {
        public int GastId { get; set; }
        public int GastTipo { get; set; }
        public string GastObse { get; set; }
        public int GastViaj { get; set; }
        //public Nullable<System.DateTime> GastFech { get; set; }
        public string GastFech { get; set; }
        public string GastUsua { get; set; }
        public int GastEmpr { get; set; }
        public double GastValo { get; set; }
        public int GastMone { get; set; }
        public string GastLugar { get; set; }
        public string GastImag { get; set; }
        public string GastCont { get; set; }
        public int GastCeco { get; set; }
        public int GastReem { get; set; }
        public string GastNomTip { get; set; }
        public string GastMoneda { get; set; }
    }
}
