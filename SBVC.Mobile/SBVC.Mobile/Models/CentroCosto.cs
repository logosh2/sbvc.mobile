﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBVC.Mobile.Models
{
    public class CentroCosto
    {
        public int CecoId { get; set; }
        public string CecoDesc { get; set; }
        public Nullable<int> CecoEmpr { get; set; }
        public Nullable<int> CecoEsta { get; set; }
        public string CecoCeco { get; set; }
        public Nullable<int> CecoUnid { get; set; }
        public Nullable<int> CecoAut1 { get; set; }
        public Nullable<int> CecoAut2 { get; set; }
        public Nullable<int> CecoTies { get; set; }
        public Nullable<int> CecoPres { get; set; }
        public string CecoObse { get; set; }
        public Nullable<int> CecoPrna { get; set; }
        public Nullable<int> CecoPrin { get; set; }
        public Nullable<int> CecoDivi { get; set; }
        public Nullable<int> CecoPadr { get; set; }
        public Nullable<int> CecoTipo { get; set; }
        public Nullable<int> CecoAut1Latam { get; set; }
        public Nullable<int> CecoAut2Latam { get; set; }
        public Nullable<int> CecoAut1NoLatam { get; set; }
        public Nullable<int> CecoAut2NoLatam { get; set; }
        public string CecoNit { get; set; }
        public string CecoCons { get; set; }
    }
}
