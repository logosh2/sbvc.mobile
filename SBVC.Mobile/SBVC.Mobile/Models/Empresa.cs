﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBVC.Mobile.Models
{
    public class Empresa
    {
        public int EmprId { get; set; }
        public string EmprNit { get; set; }
        public string EmprNomb { get; set; }
        public string EmprLogo { get; set; }
        public string EmprOfId { get; set; }
        public Nullable<int> EmprEsta { get; set; }
        public string EmprSucu { get; set; }
        public Nullable<int> EmprGDS { get; set; }
        public string EmprOfCo { get; set; }
        public Nullable<int> EmprIdCo { get; set; }
        public Nullable<int> EmprFact { get; set; }
        public string EmprTaNu { get; set; }
        public string EmprCoFa { get; set; }
        public Nullable<int> EmprOpAv { get; set; }
        public string EmprLogoSBVC { get; set; }
        public string EmprLoFi { get; set; }
        public string EmprAvia { get; set; }
        public string EmprCons { get; set; }
        public string EmprSuFa { get; set; }
    }
}
