﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBVC.Mobile.Models
{
    public class User
    {
        public int UsuaId { get; set; }
        public string UsuaUsua { get; set; }
        public string UsuaNoCo { get; set; }
        public string UsuaDocu { get; set; }
        public Nullable<int> UsuaEmpr { get; set; }
        public Nullable<int> UsuaCeco { get; set; }
        public string UsuaCelu { get; set; }
        public Nullable<int> UsuaCarg { get; set; }
        public string UsuaCodi { get; set; }
        public string UsuaDire { get; set; }
        public string UsuaCate { get; set; }
        public string UsuaNomb { get; set; }
        public string UsuaApel { get; set; }
        public Nullable<DateTime> UsuaFeNa { get; set; }
        public string UsuaNaci { get; set; }
        public Nullable<int> UsuaGene { get; set; }
        public Nullable<int> UsuaTiDo { get; set; }
        public Nullable<int> UsuaVeNit { get; set; }
    }
}
