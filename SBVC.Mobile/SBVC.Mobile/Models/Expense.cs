﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SBVC.Mobile.Models
{
    public class Expense
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string State { get; set; }

        public string StateColor { get; set; }

        public string Category { get; set; }
        public DateTime CreactionDate { get; set; }
        public string CreactionDateText { get; set; }

        public string Type { get; set; }
        public string TypeColor { get; set; }

        public string Coin { get; set; }
        public long Amount { get; set; }

        public string AmountText { get; set; }

        public string Text { get; set; }
        public string Image { get; set; }

        public ImageSource ImageShow { get; set; }

        public string Cc { get; set; }
        public long SoliId { get; set; }
        public string Company { get; set; }
        public string Repayable { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string ImgExpense { get; set; }
        public long NitInvoice { get; set; }
        public String Passenger { get; set; }

        public List<Expense> expenseList { get; set; }
    }
}