﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBVC.Mobile.Models
{
    public class TipoGasto
    {
        public int TigaId { get; set; }
        public string TigaDesc { get; set; }
        public string TigaUsua { get; set; }
        public Nullable<int> TigaEsta { get; set; }
        public Nullable<int> TigaEmpr { get; set; }
        public string TigaFech { get; set; }
    }
}
