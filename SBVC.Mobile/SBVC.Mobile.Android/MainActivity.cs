﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Com.OneSignal;
using Android;
using Plugin.Iconize;
using System.Reflection;
using NControl.Controls.Droid;
using ImageCircle.Forms.Plugin.Droid;

namespace SBVC.Mobile.Droid
{
    [Activity(Label = "SBVC.Mobile", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, LaunchMode =LaunchMode.SingleTask, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
           
            base.OnCreate(bundle);
            Iconize.With(new Plugin.Iconize.Fonts.MaterialModule()).With(new Plugin.Iconize.Fonts.FontAwesomeModule());
            OneSignal.Current.StartInit("a7b2259a-1fa5-41ae-bc95-896d5911ed9b").EndInit();
            //CurrentPlatform.Init();
            global::Xamarin.Forms.Forms.Init(this, bundle);
            NControls.Init();
            ImageCircleRenderer.Init();
            //TabLayoutResource = Resource.Layout.Tabbar;
            var cv = typeof(Xamarin.Forms.CarouselView);
            var assembly = Assembly.Load(cv.FullName);
            ToolbarResource = Resource.Layout.Toolbar;
            LoadApplication(new App());
            XFGloss.Droid.Library.Init(this, bundle);

        }
    }
}

